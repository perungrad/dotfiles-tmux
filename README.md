# Tmux Configuration

## Installation

```bash
# Download code to a new `tmux` directory inside your `~/.config`
git clone https://perun@bitbucket.org/perungrad/dotfiles-tmux.git ~/.config/tmux

# Do not forget to symlink main config file to your home directory:
ln -s ~/.config/tmux/tmux.conf ~/.tmux.conf
```
