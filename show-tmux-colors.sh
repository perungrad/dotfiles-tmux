#!/bin/bash

for i in {0..63}; do
    printf "\x1b[38;5;${i}mcolour${i}\x1b[0m "

    j=`expr $i + 64`
    printf "\x1b[38;5;${j}mcolour${j}\x1b[0m "

    j=`expr $i + 128`
    printf "\x1b[38;5;${j}mcolour${j}\x1b[0m "

    j=`expr $i + 192`
    printf "\x1b[38;5;${j}mcolour${j}\x1b[0m\n"
done
