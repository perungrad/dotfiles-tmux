const { exec } = require('child_process');
const process = require('process');

const colors = ['colour10', 'colour11', 'colour166', 'colour168', 'colour9', 'colour196'];

const getColor = (load) => {
    const n = Math.min(Math.floor(load / 2), colors.length - 1);

    return colors[n];
};

exec('cat /proc/loadavg', (error, stdout, stderr) => {
    const result = stdout.match(/^([^\s]+)\ ([^\s]+)\ ([^\s]+)/);

    if (!result) {
        return;
    }

    const load1 = parseFloat(result[1]);
    const load5 = parseFloat(result[2]);
    const load15 = parseFloat(result[3]);

    process.stdout.write(
        `#[fg=${getColor(load1)}]${result[1]}#[default] ` +
            `#[fg=${getColor(load5)}]${result[2]}#[default] ` +
            `#[fg=${getColor(load15)}]${result[3]}#[default]`
    );
});
